#include "ServerSocket.h"
#include "SocketException.h"
#include "frame.h"
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <time.h>

int COUNTER = 0;

void stringToChar(std::string m_pString, char* outStr)
{
   outStr[m_pString.length()+1];
   strcpy(outStr, m_pString.c_str());
}

std::string CalculateParity(std::string line)
{
   char pBit = '0';
   int sum = 0;

   for(int i = 0; i < line.length(); i++)
   {
      sum += line[i];
   }

   if (sum % 2 != 0)
      pBit = '1';

   if( COUNTER == 4){
      if (pBit == 0)
	 pBit = '1';
      else
	 pBit = '0';
   }

   COUNTER++;
   COUNTER = COUNTER % 5;

   std::string pline = line + pBit;
   return pline;
}

void SendNCheck(const char* book, ServerSocket& sock){
   std::string line;
   std::string line2;
   std::ifstream  infile(book);

   while(std::getline(infile, line))
   {
      std::istringstream iss(line);
      for(int i = 1; i < line.length(); i+=63){
	 line2 = line.substr(i, i + 63);
	 std::string pline = CalculateParity(line2);
	 sock << pline;

	 std::string AN;
	 sock >> AN;
	 while(AN == "NAK")
	 {
	    line2 = line2.substr(0, line2.length() - 1);
	    pline = CalculateParity(line2);
	    sock << pline;
	    sock >> AN;
	 }
      }
   }
   sock << "eof";
}

int main(int argc, char* argv[])
{
   std::cout << "running....\n";
   try{
      // Create the socket
      ServerSocket server(30000);   
      while (true){
	 ServerSocket new_sock;
	 server.accept(new_sock);
	 // For multiple threading, you need to create
	 // a new thread here and pass new_sock to it.
	 // The thread will use new_sock to communicate
	 // with the client.
	 try{
	    while (true){
	       std::string data;
	       new_sock << "What book do you want? (Pride, Frank, Christmas): ";
	       new_sock >> data;

	       if(data == "Pride" || data == "pride")
	       {
		  SendNCheck("./Text/PrideandPrejudiceNum.txt", new_sock);
	       }

	       if(data == "Frank" || data == "frank")
	       {
		  SendNCheck("./Text/FrankensteinOrTheModernPrometheusNum.txt", new_sock);
	       }

	       if(data == "Christmas" || data == "christmas")
	       {
		  SendNCheck("./Text/AChristmasCarolInProseNum.txt", new_sock);
	       }
	    }
	 }
	 catch(SocketException&){
	 }
      }
   }
   catch (SocketException& e){
      std::cout << "Exception was caught:" << e.description() << "\nExiting.\n";
   }
  return 0;
}
