# Makefile for the socket programming tutorial 
#

server = ServerSocket.o Socket.o server.o ServerSocket.h Socket.h SocketException.h frame.o frame.h
client = ClientSocket.o Socket.o client.o ServerSocket.h Socket.h SocketException.h frame.o frame.h


all : server client

server: $(server)
	g++ -std=c++11 -o server $(server) -lpthread

client: $(client)
	g++ -std=c++11 -o client $(client)

Socket: Socket.cpp 
ServerSocket: ServerSocket.cpp 
ClientSocket: ClientSocket.cpp
server: server.cpp
client: client.cpp

clean:
	rm -f *.o simple_server simple_client server client *~ *#
