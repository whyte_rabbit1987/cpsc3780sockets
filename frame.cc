#include "frame.h"

Frame :: Frame(int ftype, char arr[], int pbit)
{
   frameType = ftype;
   parityBit = pbit;
   for (int i = 0; i < maxSize; i++)
   {
      c[i] = arr[i];
   }
}

void Frame :: generateParity(char arr[])
{
   int sum = 0;
   
   for(int i = 0; i < maxSize; i++)
   {
      sum = sum + arr[i];
   }
   
   if(sum%2 != 0) //odd
      parityBit = 1;
   else //even
      parityBit = 0;
}

int Frame :: checkParity()
{
   return parityBit;
}

int Frame :: setFrameType(int type) //0 for data, 1 for ACK, 2 for NAK
{
   frameType = type;
}
