#include "ServerSocket.h"
#include "SocketException.h"
#include "frame.h"
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#define NUM_THREADS 5
ServerSocket new_sock;

int COUNTER = 0;

std::string CalculateParity(std::string line)
{
   char pBit = '0';
   int sum = 0;
   
   for(int i = 0; i < line.length(); i++)
   {
      sum += line[i];
   }

   if (sum % 2 != 0)
      pBit = '1';

   if( COUNTER == 4){
      if (pBit == 0)
	 pBit = '1';
      else
	 pBit = '0';
   }
   COUNTER++;
   COUNTER = COUNTER % 5;
   
   std::string pline = line + pBit;

   return pline;
}

struct thread_data
{
   char* BookPath;
   int ThreadID;
};

void *sendBook(void *args)
{
   std::string line;
   std::string line2;

   
   struct thread_data *data;
   data = (struct thread_data *) args;

   std::ifstream infile("./Text/PrideandPrejudiceNum.txt");
   
   while(std::getline(infile, line))
   {
      std::istringstream iss(line);
      for(int i = 1; i < line.length(); i+=63){
	 line2 = line.substr(i, i + 63);
	 std::string pline = CalculateParity(line2);
	 new_sock << pline;
	 usleep(100);
	 std::string AN;
	 
	 new_sock >> AN;
	 while(AN == "NAK")
	 {
	    line2 = line2.substr(0, line2.length() - 1);
	    pline = CalculateParity(line2);
	    new_sock << pline;
	    new_sock >> AN;
	    usleep(100);
	 }
      }
   }
   new_sock << "eof";
   
   pthread_exit(NULL);   
}
		     
int main(int argc, char* argv[])
{
   pthread_t threads[NUM_THREADS];
   struct thread_data tData[NUM_THREADS];
   
   std::cout << "running....\n";
   try{
      // Create the socket
      ServerSocket server(30000);   
      
      
      
      while (true){
	 server.accept(new_sock);

	 int returnCode = 0;
	 int currentNumThreads = 0;	 
	 
	 // For multiple threading, you need to create
	 // a new thread here and pass new_sock to it.
	 // The thread will use new_sock to communicate
	 // with the client.
	 try{
	    while (true){
	       std::string data;
	       new_sock >> data;

	       currentNumThreads += 1;
	     
	       if(currentNumThreads <= NUM_THREADS)
	       {
		  if(data == "Pride")
		  {
		     std::cout << "Thread Created for Pride " << currentNumThreads << std::endl;

		     tData[currentNumThreads].BookPath = (char*)"./Text/PrideandPrejudiceNum.txt";
		     tData[currentNumThreads].ThreadID = currentNumThreads;

		     returnCode = pthread_create(&threads[currentNumThreads], NULL, sendBook, (void *)&tData[currentNumThreads]);

		     if(returnCode)
		     {
			std::cout << "Error:unable to create thread," << returnCode << std::endl;
		     }
		  
		  }
		  if(data == "Frank")
		  {
		     std::cout << "Thread Created for Frank " << currentNumThreads << std::endl;

		     tData[currentNumThreads].BookPath = (char*)"./Text/FrankensteinOrTheModernPrometheusNum.txt";
		     tData[currentNumThreads].ThreadID = currentNumThreads;

		     returnCode = pthread_create(&threads[currentNumThreads], NULL, sendBook, (void *)&tData[currentNumThreads]);

		     if(returnCode)
		     {
			std::cout << "Error:unable to create thread," << returnCode << std::endl;
		     }
		  }
		  if(data == "Christmas")
		  {
		     std::cout << "Thread Created for Christmas " << currentNumThreads << std::endl;

		     tData[currentNumThreads].BookPath = (char*)"./Text/AChristmasCarolInProseNum.txt";
		     tData[currentNumThreads].ThreadID = currentNumThreads;

		     returnCode = pthread_create(&threads[currentNumThreads], NULL, sendBook, (void *)&tData[currentNumThreads]);

		     if(returnCode)
		     {
			std::cout << "Error:unable to create thread," << returnCode << std::endl;
		     }
		  }  
	       }
	       pthread_exit(NULL);
	    }
	 }
	 catch(SocketException& e){
	    std::cout << "Exception: " << e.description() << "\nJUNK.'n";
	 }
      }
   }
   catch (SocketException& e){
      std::cout << "Exception was caught:" << e.description() << "\nExiting.\n";
   }
  return 0;
}
