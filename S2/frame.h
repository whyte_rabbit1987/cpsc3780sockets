#ifndef FRAME_H
#define FRAME_H

const int maxSize = 64;

class Frame
{
  private:
   int frameType; //0 for data, 1 for ACK, 2 for NAK
   char c[maxSize];
   int parityBit; //0 or 1

  public:
   Frame(int ftype = 0, char arr[maxSize] = 0, int pbit = 0); //default empty data frame with even parity
   void generateParity(char arr[]);
   int checkParity();
   int setFrameType(int type); //0 or data, 1 for ACK, 2 for NAK
};

#endif
