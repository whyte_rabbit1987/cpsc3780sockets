#include "ClientSocket.h"
#include "SocketException.h"
#include "frame.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void stringToChar(std::string m_pString, char* outStr)
{
   outStr[m_pString.length()+1];

   strcpy(outStr, m_pString.c_str());
}

char CalculateParity(std::string line)
{
   char pBit = '0';
   int sum = 0;
   
   for(int i = 0; i < line.length(); i++)
   {
      sum += line[i];
   }

   if (sum % 2 != 0)
      pBit = '1';


   return pBit;
}

int main(int argc, char* argv[])
{
   try{
       std::string IPAddress;
       std::cout << "Enter the IP Address of the host: ";
       std::cin >> IPAddress;
       std::cout << std::endl;
      // Replace "localhost" with the hostname
      // that you're running your server.
      ClientSocket client_socket(IPAddress, 30000);
      std::string reply;


      // Usually in real applications, the following
      // will be put into a loop.
client_socket << "Pride";
      
      while(reply != "eof")
      {
	 try
	 {
	    
	    client_socket >> reply;
	       
	    
	 }
	 catch(SocketException&)
	 {
	 }
	 if(reply != "eof"){
	    //std::cout <<reply<<"\n";
	    char pbit = reply[reply.length() - 1];	    
	    std::string line = reply.substr(0, reply.length() - 1);
	    std::cout << "p: " <<pbit << ", line: " << CalculateParity(line) << "\n";
	    if(pbit == CalculateParity(line))
	    {
	       client_socket << "ACK";
	    }
	    else
	    {
	       client_socket << "NAK";
	    }
	    std::cout << line << "\n";
	    //std::cout << pbit << std::endl;
	 }
      }
      std::cout << "DONE";
   }
   catch(SocketException& e){
      std::cout << "Exception was caught:" << e.description() << "\n";
   }

   return 0;
}

/*<< "We received this response from the server:\n\"" */
